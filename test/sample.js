console.log('Sample unit tests running!');

// set up tests in each file like:
var assert = require('chai').assert; // choose assertion library
// put like-minded tests inside of describe blocks
describe('Sample basic tests', () => {
    // each 'it' function defines one test or 'spec'. An 'it' can contain multiple asserts.
    it('Foo should equal foo', () => {
        assert.equal("foo", "foo");
        assert.notEqual("foo", "bar");
    });
    it('Foo should not equal baz', () => {
        assert.notEqual("foo", "baz");
    });
});

// test using depencency from another file
import person from './person';
console.log(person);

describe('Sample person tests', () => {
    it('persons name should equal tom', () =>
        assert.equal(person.name, "Tom")
    );
    it('persons age should equal 49', () =>
        assert.equal(person.age, 49)
    );
});

// test async code
describe('Sample asynchronous test', () => {
    it('Should test asynchronous code', (done) => {
        setTimeout(function () {
            assert.equal(person.name, "Tom");
            done();
        }, 1000);
    });
});

// test globals
describe('Sample global variable test', () => {
    require('./global');
    it('global.foo should equal "foo"', () => {
        assert.equal("foo", global.foo);
    });
});