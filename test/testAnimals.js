import { assert } from 'chai';
import sayAnimalsName from './animals';

describe('Sample testAnimal tests', () => {
    it('should say animals name', () =>
        assert.equal(sayAnimalsName(), "dog")
    );
});