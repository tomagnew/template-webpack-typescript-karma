console.log('Mocha unit tests running!');

import './sample'; // include all tests located in sample.js
// import 'awesome-typescript-loader!./testAnimals.ts'; // include all tests located in testAnimals.js
import './testAnimals.ts'; // include all tests located in testAnimals.js
// import './testAnimals'; // include all tests located in testAnimals.js
// import '../src/modules/module_3/test'; // include all tests located in sub.js
// import '../src/modules/access/test';

// import '../src/modules/module_4/module_4_test'; // include all tests located in 

var testsContext = require.context("../src/", true, /.spec.(t|j)s$/);
testsContext.keys().forEach(testsContext);