let assert = require('chai').assert;
import text from './index';

describe('module_2 tests', () => {
    it('should return the string "This text set by modules/module2.js"', () => {
        assert.equal(text, "This text set by modules/module2.js");
    });

});