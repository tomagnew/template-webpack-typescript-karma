import {assert} from 'chai'; 
import module_4 from './index';

describe('Module_4 basic tests', () => {

    it('should write text to the page using jQuery', () => {
        assert.equal(module_4.sayMyName("Tom"), "My name is Tom");
    }); 

});