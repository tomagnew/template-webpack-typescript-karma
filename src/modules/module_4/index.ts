interface Imodule_4 {
    sayMyName(name: string): string;
}

let nameObject: Imodule_4 = {
    sayMyName(name: string) {
        return `My name is ${name}`;
    }
};

console.log('nameObject',nameObject);

export default nameObject;
