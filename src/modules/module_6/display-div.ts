import * as $ from 'jquery';

export default (text: string) : void => {
    var newDiv = $("<div>", { id: "newDiv" });
    newDiv.text(text);
    $('body').append(newDiv);
};