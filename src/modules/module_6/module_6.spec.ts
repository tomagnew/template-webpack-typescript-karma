import {assert} from 'chai'; 
import displayDiv from './display-div';

describe('Module_6 basic tests', () => {

    it('should write text to the page using jQuery', () => {
        displayDiv("This message displayed by jQuery!");
        var text = document.getElementById('newDiv').innerText;
        assert.equal(text, "This message displayed by jQuery!");
    }); 

});