console.log('module_1_test unit tests running!');
// var obj = require('./index');
import obj from './index';
// set up tests in each file like:
var assert = require('chai').assert; // choose assertion library
// put like-minded tests inside of describe blocks
describe('module_1_test basic tests', () => {
    // each 'it' function defines one test or 'spec'. An 'it' can contain multiple asserts.
    it('div2 should "This text set by modules/module_1.js"', () => {
        assert.equal(obj.div2, "This text set by modules/module_1.js");
    });

});
