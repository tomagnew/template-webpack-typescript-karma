import job from './job';

let jobDescription = (): string => {
    let desc = `${job.title} job pays ${job.pay}$/week`;
    // console.log(desc);
    return desc;
}

export default jobDescription;
