import {assert} from 'chai'; 
import module_5 from './index';

describe('Module_5 basic tests', () => {
    it('should return a function which returns job description', () => {
        assert.equal(module_5(),"programmer job pays 500$/week");
    });

});