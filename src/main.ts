console.log('main.ts is working!');

let hello = (): void => console.log('Hello World!');

// var styles = require('./styles');
import './styles';

// var text = require('./modules/module_1');
import text from './modules/module_1';

import nameObj from '../src/modules/module_4';
console.log(nameObj.sayMyName("tOMMY"));

var div1 = document.getElementById('div1');
div1.innerText = "This text set by index.js";

var div2 = document.getElementById('div2');
div2.innerText = text.div2;

var div3 = document.getElementById('div3');
div3.innerText = text.div3;

import './modules/module_6';



